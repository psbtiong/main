﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;

namespace DataLayer
{

    public class DBController
    {
        SqlConnection Sql_Connection = new SqlConnection();
        //string Sql_String_Connection = "Data Source=198.71.225.65;" + "Initial Catalog=surprise_me;" + "User id=surprisemeadmin;" + "Password=P@s$w0rd;";
        string Sql_String_Connection = ConfigurationManager.ConnectionStrings["FYPJ_Constr"].ToString();

        public DataTable Get_DataTable(SqlCommand cmd)
        {
            this.Sql_Connection.ConnectionString = this.Sql_String_Connection;
            cmd.Connection = Sql_Connection;
            cmd.Connection.Open();
            cmd.CommandTimeout = 1200;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);
            cmd.Connection.Close();
            return dt;
        }

        public Boolean Execute_Non_Query(SqlCommand cmd)
        {
            this.Sql_Connection.ConnectionString = this.Sql_String_Connection;
            cmd.Connection = Sql_Connection;
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            cmd.Connection.Close();
            return true;
        }

        public int Execute_Scalar_Query(SqlCommand cmd)
        {
            this.Sql_Connection.ConnectionString = this.Sql_String_Connection;
            cmd.Connection = Sql_Connection;
            cmd.Connection.Open();
            int Value = 0;

            if (cmd.ExecuteScalar() != null)
            {
                Value = (int)cmd.ExecuteScalar();
            }


            cmd.Connection.Close();

            return Value;
        }

        public string Execute_Scalar_Query_String(SqlCommand cmd)
        {
            this.Sql_Connection.ConnectionString = this.Sql_String_Connection;
            cmd.Connection = Sql_Connection;
            cmd.Connection.Open();

            string value = cmd.ExecuteScalar().ToString();
            cmd.Connection.Close();

            return value;
        }

        public SqlDataReader ExecuteReader(SqlCommand cmd)
        {
            this.Sql_Connection.ConnectionString = this.Sql_String_Connection;
            cmd.Connection = Sql_Connection;
            cmd.Connection.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return reader;

            }
            catch
            {
                cmd.Connection.Close();
                throw;
            }
        }


    }
}
