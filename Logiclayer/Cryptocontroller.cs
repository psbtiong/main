﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logiclayer
{
    public class Cryptocontroller
    {
        private static string strkey = "e8ffc7e42315698b16b4fc91ae78k5ec";
        private byte[] keyBytes = Encoding.UTF8.GetBytes(strkey);
        private byte[] ivBytes = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

        protected byte[] cipherData;

        public string encryptInfo(string userinfo)
        {

            cipherData = aes256.EncryptStringToBytes(userinfo, keyBytes, ivBytes);
            string userinfo64Text = Convert.ToBase64String(cipherData);
            return userinfo64Text;
        }

        //decrypt user info
        public string decryptInfo(string onlineuserinfo)
        {
            byte[] inputbytes = Convert.FromBase64String(onlineuserinfo);
            string plainText = aes256.DecryptStringFromBytes(inputbytes, keyBytes, ivBytes);
            return plainText;
        }
    }
}
